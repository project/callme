
-- SUMMARY --

Module "Call Me" provides a block through which
users can send a requests to call back.
Requests is sent to the specified email address.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure block settings in Structure » Blocks
