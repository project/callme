<?php

/**
 * Implements hook_variable_info().
 */
function callme_variable_info($options) {
  $variables['callme_email'] = array(
    'title' => t('E-mail to send notifications about new requests. To specify multiple recipients, separate each e-mail address with a comma.'),
    'type' => 'callme_email_list',
    'required' => TRUE,
    'default' => variable_get('site_mail', ini_get('sendmail_from')),
    'group' => 'callme_group',
  );
  $variables['callme_regex'] = array(
    'title' => t('Regex to validate phone number'),
    'description' => t('Default for Russia: <em>!regex</em>', array('!regex' => CALLME_REGEX)),
    'type' => 'callme_regex_string',
    'required' => TRUE,
    'default' => CALLME_REGEX,
    'group' => 'callme_group',
  );
  $variables['callme_submit'] = array(
    'title' => t('"Call me" button text'),
    'type' => 'callme_submit_text',
    'required' => TRUE,
    'default' => 'Call me',
    'group' => 'callme_group',
  );
  $variables['callme_message'] = array(
    'title' => t('Success message'),
    'type' => 'callme_message_text',
    'required' => TRUE,
    'default' => 'Thank you for treatment. We will contact you shortly.',
    'group' => 'callme_group',
  );

  return $variables;
}

function callme_variable_group_info() {
  $groups['callme_group'] = array(
    'title' => t('Call Me module settings'),
    'description' => t('Call Me module settings: email list where to send request info, regex for check phone numbers, success message text, etc.'),
    'access' => 'administer blocks',
  );

  return $groups;
}

/**
 * Implements hook_variable_type_info().
 */
function callme_variable_type_info() {
  $types['callme_email_list'] = array(
    'title' => t('List of emails'),
    'type' => 'text',
    'validate callback' => 'callme_validate_email_variable',
    'format callback' => 'callme_format_email_variable',
  );  
  $types['callme_submit_text'] = array(
    'title' => t('Submit button text type'),
    'type' => 'text',
    'validate callback' => 'callme_validate_submit_variable',
  );
  $types['callme_regex_string'] = array(
    'title' => t('Regex string type'),
    'type' => 'string',
    'validate callback' => 'callme_validate_regex_variable',
  );
  $types['callme_message_text'] = array(
    'title' => t('Message text type'),
    'type' => 'text',
    'validate callback' => 'callme_validate_message_variable',
  );

  return $types;
}
